http://graphql.org/learn/serving-over-http/
https://github.com/nuwave/lighthouse/
https://github.com/Folkloreatelier/laravel-graphql/tree/feature/relay    variables
https://github.com/Folkloreatelier/laravel-graphql/blob/feature/relay/docs/relay.md

pagination
http://graphql.org/learn/pagination/
https://github.com/Folkloreatelier/laravel-graphql/issues/187
https://dev-blog.apollodata.com/understanding-pagination-rest-graphql-and-relay-b10f835549e7
https://www.sitepoint.com/paginating-real-time-data-cursor-based-pagination/

graphql cursor pagination
- https://developers.facebook.com/docs/graph-api/overview/
- https://developers.facebook.com/docs/graph-api/using-graph-api/  (обовязково для читання)
- https://github.com/chentsulin/awesome-graphql
- https://github.com/Folkloreatelier/laravel-graphql/issues/156
- nice post  http://blog.mauriziobonani.com/first-steps-with-graphql-in-laravel-part-one/
- https://www.youtube.com/watch?v=eUpu7GOeceA&list=WL&index=260

different types of pagination
https://stackoverflow.com/questions/41418671/how-is-facebook-graph-api-pagination-works-and-how-to-iterate-facebook-user-feed
https://facebook.github.io/relay/graphql/connections.htm

edge pagination structure
If you request /me, it's a node, if you request /me/feed, it's an edge.
user is a NODE user/photos is a EDGE
https://github.com/apollographql/graphql-tools/issues/214
https://developers.facebook.com/docs/graph-api/using-graph-api/  (norm info)
create table in db
id  pagination_name  cursor   cursor_type(before after)    element_id(id of element from which start (always check if it exists))  user_id
as cursor can be: id or other unique string(which we can generate for every db row)


advanced practise in graphql
https://github.com/rebing/graphql-laravel

good explanation
https://developer.github.com/v4/guides/forming-calls/
https://developer.github.com/v4/guides/migrating-from-rest/

about http methods in graphql
https://kellysutton.com/2017/01/02/do-we-need-graphql.html

trying graphql with ralay https://facebook.github.io/relay/docs/getting-started.html
and graphql with JSON API http://jsonapi.org/

for auth we can create different schema
https://github.com/Folkloreatelier/laravel-graphql/pull/203

join monster library with best practies for graphql
https://join-monster.readthedocs.io/en/latest/
https://github.com/stems/join-monster-demo

