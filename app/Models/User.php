<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Model implements Authenticatable
{
    use AuthenticableTrait;
    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public static function isAuthorized(){
        $user = null;
        try{
            $user = JWTAuth::parseToken()->toUser();
        }catch(JWTException $e){}

        return $user;
    }

    public static function ifAdmin(){
        $user = self::isAuthorized();
        if($user){
            $exploded_roles = explode(',', $user->roles);
            if(!in_array('admin', $exploded_roles)){
                return false;
            }
            return true;
        }
        return false;
    }

}