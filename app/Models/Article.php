<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public static $LIMIT_PER_PAGE = 15;
    public static $FIRST_PAGE = 1;

}