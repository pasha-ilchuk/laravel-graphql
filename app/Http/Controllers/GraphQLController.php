<?php

namespace App\Http\Controllers;

use \Folklore\GraphQL\GraphQLController as BaseGraphQLController;
use Illuminate\Http\Request;

class GraphQLController extends BaseGraphQLController
{
    public function __construct(Request $request)
     {
         $schema = $request->route('graphql_schema') ?
         $request->route('graphql_schema') : config('graphql.schema');

         $middleware = config('graphql.middleware_schema.' . $schema, null);

         if ($middleware) {
             $this->middleware($middleware);
         }
     }
}