<?php

namespace App\Http\Middleware;

use App\Exceptions\BadTokenException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiAuthenticate
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws BadTokenException
     */
    public function handle(Request $request, \Closure $next)
    {
        $user = null;
        try{
            $user = JWTAuth::parseToken()->toUser();
        }catch(JWTException $e){
            throw new BadTokenException("Bad token. Access denied");
        }

        if(!$user){
            throw new BadTokenException("Bad token. Access denied");
        }
        return $next($request);
    }

}