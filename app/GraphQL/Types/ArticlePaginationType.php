<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ArticlePaginationType extends GraphQLType
{
    protected $attributes = [
        'name'         => 'ArticlePagination',
        'description'  => 'An article pagination item'
    ];

    public function fields(){
        return [
            'list'         => [
                'type' => Type::listOf(\GraphQL::type('Article')),
                'description' => 'List of articles'
            ],
            'total'      => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'A total count of articles'
            ]
        ];
    }

}