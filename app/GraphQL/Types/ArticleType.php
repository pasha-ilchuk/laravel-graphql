<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

/**
 * Class ArticleType
 * @package App\GraphQL\Types
 */
class ArticleType extends GraphQLType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'         => 'Article',
        'description'  => 'An article item'
    ];

    /**
     * @return array
     */
    public function fields(){
        return [
            'id'         => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'An id'
            ],
            'title'      => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'An title'
            ],
            'url'        => [
                'type' => Type::string(),
                'description' => 'An url'
            ],
            'image'      => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'An image'
            ],
            'content'    => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'An content'
            ],
            'user'       => [
                'type' => \GraphQL::type('User'),
                'description' => 'An user'
            ],
            'status'     => [
                'type' => Type::string(),
                'description' => 'An status'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Data of creation'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date of last update'
            ]
        ];
    }

}