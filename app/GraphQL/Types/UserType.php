<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    /**
     * @var array
     */
    protected $attributes = [
        'name'         => 'User',
        'description'  => 'An user item'
    ];

    /**
     * @return array
     */
    public function fields(){
        return [
            'id'         => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'An id'
            ],
            'name'      => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'An title'
            ],
            'email'        => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'An url'
            ]
        ];
    }
}