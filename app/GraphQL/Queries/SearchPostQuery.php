<?php

namespace App\GraphQL\Queries;

use App\Models\Post;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Illuminate\Support\Facades\Log;

/**
 * Class ArticlesQuery
 * @package App\GraphQL\Queries
 */
class SearchPostQuery extends Query
{
    /**
     * @return GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('Post'));
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'query'         => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Query'
            ],
            'limit'         => [
                'type' => Type::int(),
                'description' => 'Limit'
            ],
            'cursor'         => [
                'type' => Type::string(),
                'description' => 'Cursor'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return mixed
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        $cursor  = @$args['cursor'] ? $args['cursor'] : null;
        $limit = @$args['limit'] ? $args['limit'] : Post::$LIMIT_PER_PAGE;
        $cursor_id = 0;
        if($cursor){
            $cursor_id = Post::where('cursor', $cursor)->first()->id;
        }
        $query = Post::where('title','like','%' . $args['query'] . '%')->where('id', '>', $cursor_id)->limit($limit)->get();
        $result = $query->map(function (Post $post) {
            return [
                'id' => $post->id,
                'title' => $post->title,
                'image' => $post->image,
                'content' => $post->content_rendered,
                'status' => $post->status,
                'user' => [
                    'id' => $post->user->id,
                    'name' => $post->user->name,
                    'email' => $post->user->email
                ],
                'cursor' => $post->cursor
            ];
        });
        return $result;
    }

}