<?php

namespace App\GraphQL\Queries;

use App\Models\Post;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

/**
 * Class ArticlesQuery
 * @package App\GraphQL\Queries
 */
class PostQuery extends Query
{
    /**
     * @return GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('Post'));
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'limit'         => [
                'type' => Type::int(),
                'description' => 'Limit'
            ],
            'cursor'         => [
                'type' => Type::int(),
                'description' => 'Cursor'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return mixed
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        $cursor = @$args['cursor'] ? $args['cursor'] : 0;
        $limit = @$args['limit'] ? $args['limit'] : Post::$LIMIT_PER_PAGE;
        $query = Post::where('id', '>', $cursor)->limit($limit)->orderBy('id')->get();
        $result = $query->map(function (Post $post) {
            return [
                'id' => $post->id,
                'title' => $post->title,
                'image' => $post->image,
                'content' => $post->content_rendered,
                'status' => $post->status,
                'user' => [
                    'id' => $post->user->id,
                    'name' => $post->user->name,
                    'email' => $post->user->email
                ]
            ];
        });
        return $result;
    }

}