<?php

namespace App\GraphQL\Queries;

use App\Models\Article;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

/**
 * Class ArticlesQuery
 * @package App\GraphQL\Queries
 */
class ArticleQuery extends Query
{
    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return GraphQL::type('Article');
    }

    /**
     * @return array
     */
    /* can set input parameters that need */
    public function args()
    {
        return [
            'id'         => [
                'type' => Type::nonNull(Type::id()),
                'description' => 'An id'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return mixed
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        $article = Article::with('user')->where('id', $args['id'])->first();
        $result['id'] = $article->id;
        $result['title'] = $article->title;
        $result['image'] = $article->image;
        $result['content'] = $article->content_rendered;
        $result['status'] = $article->status;
        $result['user'] = [
            'id' => $article->user->id,
            'name' => $article->user->name,
            'email' => $article->user->email
        ];
        return $result;
    }

}