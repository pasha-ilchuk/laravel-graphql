<?php

namespace App\GraphQL\Queries;

use App\GraphQL\Resolvers\IsAdminResolver;
use App\Models\User;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;

/**
 * Class UsersQuery
 * @package App\GraphQL\Queries
 */
class UsersQuery extends IsAdminResolver
{
    /**
     * @return GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('User'));
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id'         => [
                'type' => Type::id(),
                'description' => 'An id'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return \Illuminate\Support\Collection
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        parent::resolve($root, $args);

        $query = User::query();

        if(@$args['id']){
            $query = $query->where('id', $args['id']);
        }

        return $query->get()->map(function(User $user){
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            ];
        });
    }

}