<?php

namespace App\GraphQL\Queries;

use App\Models\Article;
use App\Models\User;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * Class ArticlesQuery
 * @package App\GraphQL\Queries
 */
class ArticlesQuery extends Query
{
    /**
     * @return \GraphQL\Type\Definition\ListOfType
     */
    public function type()
    {
        return GraphQL::type('ArticlePagination');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'limit'         => [
                'type' => Type::int(),
                'description' => 'A limit'
            ],
            'page'         => [
                'type' => Type::int(),
                'description' => 'A page'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return mixed
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        if(Request::method() == "GET"){
            $query = Article::query();
            $is_admin = User::ifAdmin();
            if($is_admin){
                $query = $query->with('user');
            }
            $limit = @$args['limit'] ? $args['limit'] : Article::$LIMIT_PER_PAGE;
            $page = @$args['page'] ? $args['page'] : Article::$FIRST_PAGE;

            $query = $query->paginate($limit, ['*'], 'page', $page);

            // якщо поле обявлене як Type::nonNull то обовязково повинно бути оброблене тут
            // тому що якщо його користувач запросить а воно не обробляється то намагатиметься вернути null
            // і не дасть вернути null бо поле Type::nonNull і видасть помилку
            // поля які обявлені без Type::nonNull можуть не оброблятися, вони просто повернуться як null
            $result['list'] = $query->map(function (Article $article) use ($is_admin) {
                $res = [];
                $res['id'] = $article->id;
                $res['title'] = $article->title;
                $res['image'] = $article->image;
                $res['content'] = $article->content_rendered;
                $res['status'] = $article->status;
                if($is_admin){
                    $res['user'] = [
                        'id' => $article->user->id,
                        'name' => $article->user->name,
                        'email' => $article->user->email
                    ];
                }
                return $res;
                /*return [
                    'id' => $article->id,
                    'title' => $article->title,
                    'image' => $article->image,
                    'content' => $article->content_rendered,
                    'status' => $article->status,
                    'user' => [
                        'id' => $article->user->id,
                        'name' => $article->user->name,
                        'email' => $article->user->email

                ];*/
            });
            $result['total'] = $query->total();

            return $result;
        }
        throw new MethodNotAllowedHttpException(['GET'], "Method not allowed for this query");
    }

}