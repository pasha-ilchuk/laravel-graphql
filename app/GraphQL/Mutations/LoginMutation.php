<?php

namespace App\GraphQL\Mutations;

use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class LoginMutation
 * @package App\GraphQL\Mutations
 */
class LoginMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'login'
    ];

    /**
     * @return \GraphQL\Type\Definition\StringType
     */
    public function type()
    {
        return Type::string();
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'email'    => ['name' => 'email', 'type' => Type::string(), 'rules' => ['required', 'email']],
            'password' => ['name' => 'password', 'type' => Type::string(), 'rules' => ['required']]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return mixed
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        $credentials = [
            'email' => $args['email'],
            'password' => $args['password']
        ];

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                throw new AuthorizationError("Bad credentials");
            }
        } catch (JWTException $e) {
            throw new AuthorizationError($e->getMessage());
        }

        return $token;
    }
}