<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UpdateUserEmailMutation extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateUserEmail'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'email' => ['name' => 'email', 'type' => Type::string()]
        ];
    }

    public function rules()
    {
        return [
            'email' => ['required', 'email']
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return User
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        if(Request::method() == "POST") {
            $user = User::find(auth()->user()->id);
            if (!$user) {
                throw new NotFoundHttpException("User not found");
            }

            $user->email = $args['email'];
            $user->save();

            return $user;
        }
        throw new MethodNotAllowedHttpException(["POST"], "Method not allowed for this mutation");
    }
}