<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use Folklore\GraphQL\Error\AuthorizationError;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UpdateUserPasswordMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'UpdateUserPassword'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('User');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'password' => ['name' => 'password', 'type' => Type::nonNull(Type::string())]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return User
     * @throws AuthorizationError
     */
    public function resolve($root, $args)
    {
        $user = User::find(auth()->user()->id);
        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }

        $user->password = bcrypt($args['password']);
        $user->save();

        return $user;
    }
}