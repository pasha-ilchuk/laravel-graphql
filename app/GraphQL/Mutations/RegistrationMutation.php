<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;

/**
 * Class RegistrationMutation
 * @package App\GraphQL\Mutations
 */
class RegistrationMutation extends Mutation
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'registration'
    ];

    /**
     * @return mixed
     */
    public function type()
    {
        return GraphQL::type('User');
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'name'     => ['name' => 'name', 'type' => Type::string(), 'rules' => ['required']],
            'email'    => ['name' => 'email', 'type' => Type::string(), 'rules' => ['required', 'email']],
            'password' => ['name' => 'password', 'type' => Type::string(), 'rules' => ['required']],
            'roles'    => ['name' => 'roles', 'type' => Type::string(), 'rules' => ['required']]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return User
     */
    public function resolve($root, $args)
    {
        $user = new User();
        $user->name = $args['name'];
        $user->email = $args['email'];
        $user->password = bcrypt($args['password']);
        $user->roles = $args['roles'];
        $user->save();
        return $user;
    }
}