<?php

namespace App\GraphQL\Resolvers;

use App\Exceptions\BadTokenException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class IsAuthenticatedResolver extends BaseResolver
{
    /**
     * @param $root
     * @param $args
     * @return bool
     * @throws BadTokenException
     */
    public function resolve($root, $args)
    {
        parent::resolve($root, $args);

        $user = null;
        try{
            $user = JWTAuth::parseToken()->toUser();
        }catch(JWTException $e){
            throw new BadTokenException("Bad token. Access denied");
        }

        if(!$user){
            throw new BadTokenException("Bad token. Access denied");
        }

        return $user;
    }
}