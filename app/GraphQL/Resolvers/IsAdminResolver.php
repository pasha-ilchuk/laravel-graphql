<?php

namespace App\GraphQL\Resolvers;

use App\Exceptions\ForbiddenException;

class IsAdminResolver extends IsAuthenticatedResolver
{
    /**
     * @param $root
     * @param $args
     * @return bool
     * @throws ForbiddenException
     */
    public function resolve($root, $args)
    {
        $user = parent::resolve($root, $args);
        $exploded_roles = explode(',', $user->roles);
        if(!in_array('admin', $exploded_roles)){
            throw new ForbiddenException("You are not allowed to do that. Access denied");
        }
        return true;
    }
}