<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $t) {
            $t->increments('id');
            $t->unsignedInteger('user_id')->index();
            $t->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $t->string('title');
            $t->string('image');
            $t->string('slug')->unique()->index();
            $t->longText('content_source');
            $t->longText('content_rendered');
            $t->enum('status', [
                'Draft',
                'Review',
                'Published',
            ])->default('Draft');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
